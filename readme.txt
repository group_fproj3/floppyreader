PROJECT 3 - GROUP F
===================

Project Description: In this assignment, we
are writing a program that allows users to access a floppy disk 
locally mounted on a computer. We are expected to use C programming language. 
In our program, all file I/O related operations should use the system calls discussed 
in our lectures, including open(), read(), write(), lseek(), close().

Group Members' Contributions:
--------------------------
Alex Demko: 33 1/3%
Pierre Jackson: 33 1/3%
Andrew Yu: 33 1/3%

Compiling Instruction: make

Sample Test Run:

vangogh:~/Desktop% ./floppymain

flop:fmount imagefile.img
imagefile.img mounted.

flop:structure
		number of FATs:   		  2
		number of sectors used by FAT:    9
		number of sectors per cluster:    1
		number of ROOT entries:         224
		number of bytes per sector:     512
		---Sector #---     ---Sector Types---
		   0			 BOOT
		 1 -- 9			 FAT1
		10 -- 18		 FAT2
		19 -- 32		 ROOT DIRECTORY

flop:help
fmount [filename] - mount a local floppy disk
fumount - unmount the mounted floppy disk
structure - list the structure of the floppy disk
traverse - list the content in the root directory
traverse -l - list the content in the root directory with additional details
showsector [number] - show the contents of given sector in the form of a hex dump
showfile [filename] - show the contents of given file in the form of a hex dump

flop: showfat

      0    1    2    3    4    5    6    7    8    9    A    B    C    D    E    F

10              free 004  FFF  006  FFF  FFF  009  FFF  FFF  00C  FFF  FFF  FFF  010  
20    011  FFF  FFF  FFF  015  FFF  017  FFF  FFF  FFF  FFF  FFF  01D  FFF  01F  020  
30    FFF  FFF  free free free free free free free free free free free free free free 
40    free free free free free free free free free free free free free free free free 
50    free free free free free free free free free free free free free free free free 
60    free free free free free free free free free free free free free free free free 
70    free free free free free free free free free free free free free free free free 
80    free free free free free free free free free free free free free free free free 
90    free free free free free free free free free free free free free free free free 
A0    free free free free free free free free free free free free free free free free 
B0    free free free free free free free free free free free free free free free free 
C0    free free free free free free free free free free free free free free free free 
D0    free free free free free free free free free free free free free free free free 
E0    free free free free free free free free free free free free free free free free 
F0    free free free free free free free free free free free free free free free free 


flop:showsector 10

     0   1   2   3   4   5   6   7   8   9   A   B   C   D   E   F

00  F0  FF  FF  00  40  00  FF  6F  00  FF  FF  FF  09  F0  FF  FF  
01  CF  00  FF  FF  FF  FF  0F  01  11  F0  FF  FF  FF  FF  15  F0  
02  FF  17  F0  FF  FF  FF  FF  FF  FF  FF  1D  F0  FF  1F  00  02  
03  FF  FF  FF  00  00  00  00  00  00  00  00  00  00  00  00  00  
04  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  
05  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  
06  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  
07  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  
08  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  
09  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  
0A  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  
0B  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  
0C  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  
0D  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  
0E  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  
0F  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  
10  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  
11  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  
12  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  
13  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  
14  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  
15  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  
16  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  
17  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  
18  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  
19  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  
1A  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  
1B  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  
1C  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  
1D  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  
1E  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  
1F  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  00  

flop:traverse

/A.CLA
/ALOOP1.CLA
/ALOOP1.JAV
/ALOOP2.CLA
/ALOOP2.JAV
/ALOOP3.CLA
/ALOOP3.JAV
/B.CLA
/CONDIT.CLA
/CONDIT.JAV
/D.CLA
/DEMO.CLA
/DEMO.JAV
/DIR1     				<DIR>
/DIR1/.     				<DIR>
/DIR1/DIR2     				<DIR>
/DIR2/.     				<DIR>
/DIR2/..     				<DIR>
/DIR2/FILE3

/DIR1/C.CLA

/POLYTEST.CLA
/POLYTEST.JAV
/SOME.CLA


flop:traverse -l


   *****************************
   ** FILE ATTRIBUTE NOTATION **
   **                         **
   ** R ------ READ ONLY FILE **
   ** S ------ SYSTEM FILE    **
   ** H ------ HIDDEN FILE    **
   ** A ------ ARCHIVE FILE   **
   *****************************
-A---    10/27/2014 13:13:05      670  	/A.CLA          			  3
-A---    10/27/2014 13:13:05      763  	/ALOOP1.CLA          			  5
-A---    10/27/2014 13:13:05      333  	/ALOOP1.JAV          			  7
-A---    10/27/2014 13:13:05      786  	/ALOOP2.CLA          			  8
-A---    10/27/2014 13:13:05      404  	/ALOOP2.JAV          			 10
-A---    10/27/2014 13:13:05      786  	/ALOOP3.CLA          			 11
-A---    10/27/2014 13:13:05      408  	/ALOOP3.JAV          			 13
-A---    10/27/2014 13:13:05      358  	/B.CLA          			 14
-A---    10/27/2014 13:13:05     1067  	/CONDIT.CLA          			 15
-A---    10/27/2014 13:13:05      444  	/CONDIT.JAV          			 18
-A---    10/27/2014 13:13:05      508  	/D.CLA          			 19
-A---    10/27/2014 13:13:05      967  	/DEMO.CLA          			 20
-A---    10/27/2014 13:13:05      648  	/DEMO.JAV          			 22
-----    10/27/2014 13:13:05    <DIR>   /DIR1
-----    10/27/2014 13:13:05    <DIR>   /DIR1/.          			 24
-----    10/27/2014 13:13:05    <DIR>   /DIR1/DIR2
-----    10/27/2014 13:13:05    <DIR>   /DIR2/.          			 25
-----    10/27/2014 13:13:05    <DIR>   /DIR2/..          			 24
-A---    10/27/2014 13:13:05       11  	/DIR2/FILE3          			 26
          			 25
-A---    10/27/2014 13:13:05      427  	/DIR1/C.CLA          			 27
          			 24
-A---    10/27/2014 13:13:05      630  	/POLYTEST.CLA          			 28
-A---    10/27/2014 13:13:05     1063  	/POLYTEST.JAV          			 30
-A---    10/27/2014 13:13:05      226  	/SOME.CLA          			 33





Existing Bugs:
 
Fmount doesn't have a way of telling you when it hasn't mounted a file! (i.e file not found.)
If the string "quit" is found anywhere in user input, even in a filename, the program exits.