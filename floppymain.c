#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <floppy.h>


int main() {
	char input[99];	//two input strings for interpreting user input
	char * input2 = malloc(sizeof(*input2));
	char t;

   userinput++;
   
   while (userinput) {
	memset(input, 0, sizeof(input));
	memset(input2, 0, sizeof(input2));	
	printf("\nflop:");
	scanf("%[^\n]s", input);
        while((t = getchar()) != '\n' && t != EOF);   //manually flush std buffer!

	/* quit the shell */
	if(strstr(input, "quit")!= NULL){
		puts("");
		return 0;
	}
		
	if(strstr(input, "help")!= NULL){
		help();
		continue;
	}
	
	if(strstr(input, "fmount")!= NULL) {
		if(setfloppy != 0) {		
			printf("Floppy already mounted. Unmount current floppy if you wish to mount a new floppy.");
			continue;
		} else {
			input2 = getArguments(input);
			fmount(input2); //will create a string with no spaces then point to the part after "fmount"
			continue;	
		}
	}

	if(strstr(input, "fumount")) {
		if(setfloppy != 0) {		
			fumount(); 
			continue;
		} else {	
		 	printf("There is no floppy mounted.\n");
			continue;	
		}	
	}
	

	if(strstr(input, "structure")) {
		if(setfloppy != 0) {		
			structure(); 
			continue;
		} else {	
		 	printf("There is no floppy mounted.\n");
			continue;	
		}	
	}

	/* test routines for debugging */
	if (strstr(input, "test")){
		printf("This is a test.\n");
		

	}
	//if strcmp traverse then you need to check if it has -1 arg or not before knowing which traverse to call!
	

	
































	/* final else statement, goes back to user input. */
	else {
		printf("command not found. Type \"help\" to see valid commands.");
		
	}
   

   }//end while loop

}