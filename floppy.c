#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <floppy.h>





/* mounts (opens) floppy image. */
void fmount(char * file) {
	if (!setfloppy){
	fd = open(file, O_RDONLY,0);
		lseek(fd, 0,SEEK_SET);
		read(fd, currentBuffer,512);
	}
	initializeFloppy();	
	setfloppy =1;
	floppyfilename = file;
	printf("%s mounted.\n", (file));
}

/*closes filestream, clears set floppy status, erases floppyname */
void fumount() {
	close(floppyfilename);
	setfloppy =0;
	printf("%s unmounted.\n", floppyfilename);
	floppyfilename[0] = '\0';
	}	

/* will print floppy info via relevant STRUCT values */
void structure(){
	printf("\t\tnumber of FATs: \t\t%d\n", floppyA.numberoffats);
	printf("\t\tnumber of sectors used by FAT: %d\n",floppyA.sectperfat );
	printf("\t\tnumber of sectors per cluster: %d\n", floppyA.sectorpercluster);
	printf("\t\tnumber of ROOT entries:         %d\n",floppyA.maximumroot);
	printf("\t\tnumber of bytes per sector:    %d\n",SECTOR_SIZE);
	printf("\t\t---Sector #---     ---Sector Types---\n");
	printf("\t\t   0\t\t\t BOOT\n");
	printf("\t\t%d -- %d\t\t\t FAT1\n", floppyA.sectorpercluster, floppyA.sectperfat);
	printf("\t\t%d -- %d\t\t FAT2\n", floppyA.sectperfat+1, floppyA.sectperfat*2);
	printf("\t\t%d -- %d\t\t ROOT DIRECTORY\n", (floppyA.sectperfat*2)+1, (floppyA.sectperfat*2)+13);
}

 /* gets all the relevant floppy info from the boot sector of the volume. */
void initializeFloppy() {
	floppyA.numberoffats = currentBuffer[16];
	floppyA.sectperfat = currentBuffer[22];
	floppyA.sectorpercluster = currentBuffer[13];
	floppyA.bytespsector = currentBuffer[11];
	floppyA.maximumroot = currentBuffer[17];
	floppyA.totalnumofsectors = currentBuffer[19];
	floppyA.sectpertrack = currentBuffer[24];
	floppyA.resersector = currentBuffer[14];
	strncpy(floppyA.label, currentBuffer+43, 11); // gets volume label for floppyA.label (use sscanf or?)
	/* get size of sectors */
	SECTOR_SIZE = (currentBuffer[12]*256);

}

void help() {
	printf("fmount [filename] - mount a local floppy disk\n");
	printf("fumount - unmount the mounted floppy disk\n");
	printf("structure - list the structure of the floppy disk\n");
	printf("traverse - list the content in the root directory\n");
	printf("traverse -l - list the content in the root directory with additional details\n");
	printf("showsector [number] - show the contents of given sector in the form of a hex dump\n");
	printf("showfile [filename] - show the contents of given file in the form of a hex dump\n");

}

char * getArguments(char * args) {
	char * arg2 = malloc(sizeof(*args));
	int len, m;
	int o = 1;
	len = (unsigned int)strlen(args);
	
	for (m = 0; m < len; m++){
		if(isspace(args[m])) {
			strncpy(arg2, (args+o), (len - m));	  // read (stringlength - current offset)
			return arg2;		
		} else {  o++; }	
			
	}
	return arg2;
}


/* will remove spaces. */
char *removeSpaces(char * str) {
	
	char * str2 = malloc(sizeof(*str2));	
	char *a = (char*)str;  
  	char *b = (char*)str2;

	do {  
  		if (*a != ' ')  
  		  *(b++) = *a;
	} while (*(a++)); 
		
	return str2;
}












