#ifndef __FLOPPY_H__
#define __FLOPPY_H__


int fd, SECTOR_SIZE, READ_COUNT, COUNTER;

int setfloppy, userinput, currentfloppy;   //these ints behave as booleans

unsigned char currentBuffer[512], secondBuffer[512];

char * floppyfilename;     //name of said floppy filename

typedef struct floppy {
unsigned int bytespsector;	//boot sector byte#11 total number of bytes per sector
unsigned int sectorpercluster;	//boot sector byte#13 number of sectors per cluster
unsigned int maximumroot;	//boot sector byte#17 maximum number of root directory entries.
unsigned int totalnumofsectors;	//boot sector byte#19 total number of sectors
unsigned int sectperfat;	//boot sector byte#22 number of sectors per FAT
unsigned int sectpertrack;	//boot sector byte#24 number of sectors per track
unsigned int resersector;	//boot sector byte#14 number of reserved sectors
unsigned int numberoffats;	//boot sector byte#16 number of FATs
unsigned char label[11];	//boot sector byte#43 volume label

} floppydefault;

struct floppy floppyA;

// SOME FUNCTIONS
void fmount(char * file);
void fumount();
//void traverse(char *row2);
void structure();
//void showsector(char *row2);
//void showfat(char *row2);
char *removeSpaces(char * str);
void help();
void initializeFloppy();
char * getArguments(char * args);

#endif